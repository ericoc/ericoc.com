Eric O'Callaghan

Philadelphia, PA
eric@ericoc.com


CERTIFICATIONS

* CompTIA Security+, Network+, and Linux+
* Hurricane Electric IPv6 Sage
* Cisco Certified Entry Networking Technician (CCENT)


BACKGROUND

* Regularly enjoy learning and playing with new things to further my own personal, technical knowledge
* Self-taught HTML, CSS, PHP, MySQL, Bash, and very familiar with Python as well as Perl
* Experience using and configuring many Linux network services such as httpd (Apache), nginx, lighttpd, iptables, ntpd, mysqld, bind/named, pptpd, openvpn, postfix, vsftpd, puppet, ansible, and sshd
* Worked with both Cisco Systems routing and switching equipment and very familiar with the Cisco Internetwork Operating System (IOS)


EXPERIENCE

Sidecar						Philadelphia, PA
Linux Systems Administrator			November 2013 - Present

- Primary duties include response to any infrastructure issues as well as decision-making for all server modifications
- Responsible for maintaining multiple separate database environments that each include replication configurations between co-located hardware and Amazon Web Services (AWS)

URBN, Inc					Philadelphia, PA
Linux Systems Administrator			September 2012 - November 2013

- Worked with a team to deploy, configure, manage, and maintain servers responsible for hosting UrbanOutfitters.com as well as Anthropologie.com
- Participated in an 24/7 on-call rotation responsible for maintaining any and all operational aspects of the e-commerce infrastructure for the above two websites

Linode, LLC					Galloway, NJ
Linux Technical Support				July 2010 - August 2012

- Provided quick and helpful technical support to a large number of customers via telephone, e-mail, and support tickets which involved troubleshooting a wide variety of issues including routing inconsistencies between numerous transit providers across the world
- Regularly communicated with international datacenters while reacting quickly to service-affecting emergencies

IT Intern					Newport News, VA
Patient Advocate Foundation			March - April 2010

- Created multiple, interactive web applications using PHP and MySQL which eased the management of data for both the Lead Web Developer and Human Resources department
- Helped set up a small, on-site datacenter as the company moved to a new office location

Directorate of Information Management		Fort Eustis, VA
Office Automation Clerk				June - September 2008

- Enjoyed management of information technology in a government facility (Department of Defense / U.S. Army) while assisting both the Network Team and Help Desk as a temporary summer hire
- Introduced to a large network, spanning the military installation, which required routine installation, maintenance, and configuration of Cisco equipment


EDUCATION

ECPI University					Newport News, VA
Networking & Security Management (A.A.S.)	September 2008 - May 2010
